mod aes_decrypt;
mod aes_encrypt;
mod svg_elems;
mod aes_test_data;

use aes_encrypt::aes_128_encrypt;
use aes_test_data::{TEST_KEY, TEST_STATE};
use svg_elems::create_svg_grid;
use wasm_bindgen::prelude::*;

// Called when the Wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global window exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    let paragraph = document.create_element("p")?;
    paragraph.set_inner_html("Hello from Rust! 🦀👋</br>This was added from <b>lib.rs</b>");

    body.append_child(&paragraph)?;

    let header = document.create_element("h2")?;
    header.set_inner_html("Adding an SVG from Rust");
    body.append_child(&header)?;

    let ciphertext = aes_128_encrypt(TEST_STATE, TEST_KEY);

    let grid = create_svg_grid(
        &document,
        // Bytes of the ciphertext grid formatted as "03", "5d", "ff", for example
        &ciphertext.map(|row| row.map(|byte| format!("{byte:02x?} "))),
    )?;

    body.append_child(&grid)?;

    Ok(())
}

#[wasm_bindgen]
pub fn encrypt_with_aes_128() -> Vec<u8> {
    let ciphertext_matrix: [[u8; 4]; 4] = aes_128_encrypt(TEST_STATE, TEST_KEY);

    let mut ciphertext_vec = vec![];

    // The ciphertext matrix is column-first
    for col_idx in 0..4 {
        for row_idx in 0..4 {
            ciphertext_vec.push(ciphertext_matrix[row_idx][col_idx]);
        }
    }

    ciphertext_vec
}
