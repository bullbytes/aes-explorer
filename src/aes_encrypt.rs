use std::iter::zip;

pub fn aes_128_encrypt(initial_state: [u8; 16], key: [u8; 16]) -> [[u8; 4]; 4] {
    println!("Hello AES-128");

    // The Rijndael substitution box, used in step SubBytes and for key expansion
    let s_box: [u8; 256] = [
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab,
        0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4,
        0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71,
        0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2,
        0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6,
        0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb,
        0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45,
        0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
        0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44,
        0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a,
        0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49,
        0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d,
        0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25,
        0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e,
        0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1,
        0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb,
        0x16,
    ];

    // Fill up the matrix one column after the other with the input bytes
    let mut state: [[u8; 4]; 4] = to_column_order_matrix(initial_state);
    println!("Initial state:");
    print_matrix(state);

    // Creating the cipher key from a password is not specified in AES. Per default, GPG adds a
    // salt to the password and hashes that with AES-128 to get the 16-byte / 128-bit cipher key.
    // GPG stores the salt inside a header before the ciphertext.
    let mut round_key: [[u8; 4]; 4] = to_column_order_matrix(key);

    // The input is XORed with the cipher key before the first full round begins
    state = xor(state, round_key);

    // The number of rounds where the state is changed via SubBytes, ShiftRows, MixColumns, and
    // AddRoundKey
    let nr_of_full_rounds = 9;

    // Round constant index starts with 1, used to get next round key
    for round_idx in 1..=nr_of_full_rounds {
        round_key = next_round_key(round_key, s_box, round_idx);
        state = xor(round_key, mix_columns(shift_rows(sub_bytes(state, s_box))));

        // println!("Round {}", round_idx);
        // println!("Current state:");
        // print_matrix(state);
        //
        // state = sub_bytes(state, s_box);
        // println!("State after S-box substitution:");
        // print_matrix(state);
        //
        // state = shift_rows(state);
        // println!("State after shifting rows:");
        // print_matrix(state);
        //
        // state = mix_columns(state);
        // println!("State after mixing columns:");
        // print_matrix(state);
        //
        // round_key = next_round_key(round_key, s_box, round_idx);
        // println!("Next round key:");
        // print_matrix(round_key);
        //
        // state = xor(state, round_key);
    }

    // The final round omits step MixColumns
    state = shift_rows(state);
    state = sub_bytes(state, s_box);
    state = xor(
        state,
        next_round_key(round_key, s_box, nr_of_full_rounds + 1),
    );
    println!("Ciphertext:");
    print_matrix(state);
    state
}

#[test]
fn aes_128_encrypt_works() {
    use crate::aes_test_data;

    let state = aes_test_data::TEST_STATE;
    let key = aes_test_data::TEST_KEY;
    let ciphertext = aes_128_encrypt(state, key);
    // From https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf
    let expected_ciphertext: [[u8; 4]; 4] = [
        [0x39, 0x02, 0xdc, 0x19],
        [0x25, 0xdc, 0x11, 0x6a],
        [0x84, 0x09, 0x85, 0x0b],
        [0x1d, 0xfb, 0x97, 0x32],
    ];
    assert_eq!(expected_ciphertext, ciphertext);
}

/// Creates the next round key for AES-128 from the `cur_key`.
fn next_round_key(cur_key: [[u8; 4]; 4], s_box: [u8; 256], round_constant_idx: u8) -> [[u8; 4]; 4] {
    /// XOR two four-byte words.
    fn xor(w1: [u8; 4], w2: [u8; 4]) -> [u8; 4] {
        zip(w1, w2)
            .map(|(b1, b2)| b1 ^ b2)
            .collect::<Vec<u8>>()
            .try_into()
            .unwrap()
    }

    // Create a mutable copy of the current round key
    let mut next_round_key = cur_key;

    // A column is one word which is four bytes
    let last_word = get_column(cur_key.len() - 1, cur_key);
    let last_word_after_g = g(last_word, round_constant_idx, s_box);

    let first_word = get_column(0, cur_key);
    let new_first_word = xor(first_word, last_word_after_g);
    set_column(0, &mut next_round_key, new_first_word);

    // If the cipher key has four columns, the col_idx goes from 0 to 2 since we XOR
    // the column at col_idx with the next column.
    for col_idx in 0..cur_key.len() - 1 {
        let cur_col = get_column(col_idx, next_round_key);
        let next_col = get_column(col_idx + 1, next_round_key);
        let new_col = xor(cur_col, next_col);
        set_column(col_idx + 1, &mut next_round_key, new_col);
    }
    next_round_key
}

/// Left-shift a word, substitute its bytes using the S-box, and XOR the result with Rcon.
/// Used for key expansion.
fn g(word: [u8; 4], round_constant_idx: u8, s_box: [u8; 256]) -> [u8; 4] {
    // Called RotWord in the standard
    let shifted = shift_left(word, 1);

    // Called SubWord in the standard
    let substituted = shifted.map(|byte| s_box[byte as usize]);

    // Only the first byte of the four-byte word is XORed with the round constant
    [
        substituted[0] ^ rcon(round_constant_idx),
        substituted[1],
        substituted[2],
        substituted[3],
    ]
}

/// Gets the round constant
fn rcon(index: u8) -> u8 {
    // We start with 0xCB because left shifting and reducing it
    // produces 0x8D, the result of rcon(0), which when left-shifted and reduced again, is 0x01.
    // And 0x01 is the correct result of rcon(1).
    let mut rcon: u16 = 0xCB;

    // If index is 0, we do this once
    for _ in 0..=index {
        rcon <<= 1;

        // If rcon is greater than a byte, we reduce with 0x11B to stay inside
        // Rijndael's Galois field
        if rcon > 0xFF {
            rcon ^= 0x11B;
        }
    }
    rcon as u8
}

#[test]
fn rcon_works() {
    assert_eq!(0x8d, rcon(0));
    assert_eq!(0x01, rcon(1));
    assert_eq!(0x02, rcon(2));
    assert_eq!(0x04, rcon(3));
    assert_eq!(0x08, rcon(4));
    assert_eq!(0x10, rcon(5));
    assert_eq!(0x20, rcon(6));
    assert_eq!(0x40, rcon(7));
    assert_eq!(0x80, rcon(8));
    assert_eq!(0x1B, rcon(9));
    assert_eq!(0x36, rcon(10));
}

/// MixColumns: The four bytes of each column of the state are modulo multiplied in Rijndael's Galois Field
/// by a given matrix
fn mix_columns(state: [[u8; 4]; 4]) -> [[u8; 4]; 4] {
    let new_state = &mut [[0; 4]; 4];

    for state_col_idx in 0..4 {
        let new_state_col: [u8; 4] =
            dot_multiply_with_mix_columns_matrix(get_column(state_col_idx, state));

        set_column(state_col_idx, new_state, new_state_col);
    }
    *new_state
}

/// Sets the column `col` at the `col_idx` inside the `matrix`. This mutates `matrix`.
fn set_column(col_idx: usize, matrix: &mut [[u8; 4]; 4], col: [u8; 4]) {
    for row_idx in 0..4 {
        matrix[row_idx][col_idx] = col[row_idx];
    }
}

/// Dot multiplies the given `state_column` with the MixColumns matrix and returns the resulting column.
///
/// For example:
/// [02 03 01 01]   d4   04
/// [01 02 03 01] · bf = 66
/// [01 01 02 03]   5d   81
/// [03 01 01 02]   30   e5
fn dot_multiply_with_mix_columns_matrix(state_column: [u8; 4]) -> [u8; 4] {
    let mix_columns_matrix = [[2, 3, 1, 1], [1, 2, 3, 1], [1, 1, 2, 3], [3, 1, 1, 2]];
    let mut new_state_col = [0; 4];
    for (row_idx, cur_matrix_row) in mix_columns_matrix.iter().enumerate() {
        let new_byte = dot_multiply_arrays(*cur_matrix_row, state_column);
        new_state_col[row_idx] = new_byte;
    }
    new_state_col
}

/// Gets a column from a matrix.
fn get_column(col_idx: usize, matrix: [[u8; 4]; 4]) -> [u8; 4] {
    let mut col = [0; 4];

    for (row_idx, row) in matrix.iter().enumerate() {
        col[row_idx] = row[col_idx];
    }
    col
}

#[test]
fn get_column_works() {
    let test_matrix = [
        [0xd4, 0xe0, 0xb8, 0x1e],
        [0xbf, 0xb4, 0x41, 0x27],
        [0x5d, 0x52, 0x11, 0x98],
        [0x30, 0xae, 0xf1, 0xe5],
    ];
    let expected_first_col = [0xd4, 0xbf, 0x5d, 0x30];
    assert_eq!(expected_first_col, get_column(0, test_matrix));
    let expected_second_col = [0xe0, 0xb4, 0x52, 0xae];
    assert_eq!(expected_second_col, get_column(1, test_matrix));
    let expected_third_col = [0xb8, 0x41, 0x11, 0xf1];
    assert_eq!(expected_third_col, get_column(2, test_matrix));
    let expected_fourth_col = [0x1e, 0x27, 0x98, 0xe5];
    assert_eq!(expected_fourth_col, get_column(3, test_matrix));
}

/// Create the dot product of a row of the MixColumns matrix and a column from the state
/// For example
/// [02 03 01 01] · [d4 bf 5d 30] =  04
fn dot_multiply_arrays(matrix_row: [u8; 4], state_column: [u8; 4]) -> u8 {
    let mut product = 0;
    for idx in 0..4 {
        product ^= gmul(matrix_row[idx], state_column[idx]);
    }
    product
}

#[test]
fn dot_multiply_arrays_works() {
    let first_row_from_mix_columns_matrix = [0x02, 0x03, 0x01, 0x1];
    let state_column = [0xd4, 0xbf, 0x5d, 0x30];
    assert_eq!(
        0x04,
        dot_multiply_arrays(first_row_from_mix_columns_matrix, state_column)
    );
    let second_row_from_mix_columns_matrix = [0x01, 0x02, 0x03, 0x01];
    assert_eq!(
        0x66,
        dot_multiply_arrays(second_row_from_mix_columns_matrix, state_column)
    );
    let third_row_from_mix_columns_matrix = [0x01, 0x01, 0x02, 0x03];
    assert_eq!(
        0x81,
        dot_multiply_arrays(third_row_from_mix_columns_matrix, state_column)
    );
    let fourth_row_from_mix_columns_matrix = [0x03, 0x01, 0x01, 0x02];
    assert_eq!(
        0xe5,
        dot_multiply_arrays(fourth_row_from_mix_columns_matrix, state_column)
    );
}

/// Gets the dot product of two bytes in Rijndael's Galois field.
fn gmul(a: u8, b: u8) -> u8 {
    let mut res: u8 = 0;
    let mut cur_a: u8 = a;
    let mut cur_b: u8 = b;

    // Go through each bit of cur_b
    for _ in 0..8 {
        // When the least significant bit of cur_b is set, we add cur_a to the result
        if cur_b & 1 == 1 {
            res ^= cur_a;
        }
        // Before left-shifting cur_a, we store whether the highest bit of cur_a is 1, meaning cur_a would be larger than a byte if we'd use a bigger type than u8.
        let high_bit_was_1 = (0b1000_0000 & cur_a) != 0;
        // Shift bits to the left, this is multiplying by x in the Galois field
        cur_a <<= 1;
        // If cur_a would be larger than a byte if using a bigger type than u8, we need to reduce using m(x) which is 0x11b normally,
        // but since cur_a is only a byte and its left-most 1 was removed in the left shift, we get rid of the second byte of 0x11b
        // and reduce with only 0x1b.
        if high_bit_was_1 {
            cur_a ^= 0x1B;
        }
        // Shift cur_b to the right to look at the next least significant bit in the
        // next round
        cur_b >>= 1;
    }
    res
}

#[test]
fn gmul_works() {
    assert_eq!(0xB3, gmul(0xD4, 0x02));
    assert_eq!(0x85, gmul(0x5C, 0x1B));
    assert_eq!(0xFE, gmul(0x57, 0x13));
    assert_eq!(0xAE, gmul(0x57, 0x02));
}

/// Shifts the rows of the `state`. The row at index 0 is shifted 0 times to
/// the left (doing nothing), the row at index 1 is shifted 1 time, and so forth.
fn shift_rows(state: [[u8; 4]; 4]) -> [[u8; 4]; 4] {
    let mut shifted = [[0; 4]; 4];

    for (row_idx, row) in state.iter().enumerate() {
        // The row_idx defines how many bytes we shift
        let shifted_row = shift_left(*row, row_idx);
        shifted[row_idx] = shifted_row;
    }

    shifted
}

/// Shifts four bytes to the left, `shift_amount` times.
/// The leftmost byte becomes the rightmost byte after a shift.
fn shift_left(bytes: [u8; 4], shift_amount: usize) -> [u8; 4] {
    let mut shifted = [0; 4];

    for (idx, val) in bytes.iter().enumerate() {
        // Since idx is unsigned, we add to it, instead of subtracting
        let right_shift = bytes.len() - shift_amount;
        let new_idx = (idx + right_shift) % bytes.len();
        shifted[new_idx] = *val;
    }
    shifted
}

/// Substitutes the bytes of the `state` using the `s_box`.
fn sub_bytes(state: [[u8; 4]; 4], s_box: [u8; 256]) -> [[u8; 4]; 4] {
    let mut state_after_s_box = [[0; 4]; 4];
    for (row_idx, row) in state.iter().enumerate() {
        for (col_idx, byte) in row.iter().enumerate() {
            state_after_s_box[row_idx][col_idx] = s_box[*byte as usize];
        }
    }
    state_after_s_box
}

/// Creates a matrix from the input array, filling the first column from the top first, then the second
/// column, till the fourth column is full.
///
/// For example:
/// Input:
/// 32, 43, f6, a8, 88, 5a, 30, 8d, 31, 31, 98, a2, e0, 37, 07, 34
///
/// Output:
/// 32 88 31 e0
/// 43 5a 31 37
/// f6 30 98 07
/// a8 8d a2 34
fn to_column_order_matrix(input: [u8; 16]) -> [[u8; 4]; 4] {
    let mut res = [[0; 4]; 4];

    let mut row_idx: usize = 0;
    let mut col_idx: usize = 0;
    for input_byte in input {
        res[row_idx][col_idx] = input_byte;

        row_idx += 1;
        if row_idx > 3 {
            row_idx = 0;
            col_idx += 1;
        }
    }
    res
}

/// XORs the bytes of two matrices.
fn xor(m1: [[u8; 4]; 4], m2: [[u8; 4]; 4]) -> [[u8; 4]; 4] {
    let mut res = [[0; 4]; 4];
    for row_idx in 0..m1.len() {
        for col_idx in 0..m1[row_idx].len() {
            res[row_idx][col_idx] = m1[row_idx][col_idx] ^ m2[row_idx][col_idx];
        }
    }
    res
}

/// Prints the contents of a two-dimensional array.
fn print_matrix(matrix: [[u8; 4]; 4]) {
    for (row_idx, row) in matrix.iter().enumerate() {
        print!("row {row_idx}: ");
        for elem in row.iter() {
            print!("{elem:02x?} ");
        }
        // New line after row is complete
        println!();
    }
}
