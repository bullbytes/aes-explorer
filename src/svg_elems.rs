use wasm_bindgen::prelude::*;
use web_sys::{Document, Element};

fn create_svg_text(
    text: &str,
    text_x: usize,
    text_y: usize,
    class: &str,
    document: &Document,
) -> Result<web_sys::Element, JsValue> {
    let svg = create_svg_elem("text", document)?;
    svg.set_text_content(Some(text));
    svg.set_attribute("x", &text_x.to_string())?;
    svg.set_attribute("y", &text_y.to_string())?;
    svg.set_attribute("class", class)?;
    Ok(svg)
}

fn create_svg_elem(elem_type: &str, document: &Document) -> Result<Element, JsValue> {
    document.create_element_ns(Some("http://www.w3.org/2000/svg"), elem_type)
}

pub(crate) fn create_svg_grid(
    document: &Document,
    grid_content: &[[String; 4]; 4],
) -> Result<Element, JsValue> {
    let grid = create_svg_elem("svg", &document)?;

    let text_style = create_svg_elem("style", &document)?;
    let text_style_class_name = "centeredText";
    text_style.set_inner_html(
        format!(".{} {{font: 24px sans-serif; fill: RoyalBlue; text-anchor: middle; dominant-baseline: middle}}", text_style_class_name).as_str());
    grid.append_child(&text_style)?;

    let start_x = 20;
    let start_y = 20;
    let rect_width = 60;
    let rect_height = 60;

    let nr_of_rows = grid_content.len();
    let nr_of_columns = grid_content[0].len();

    let horizontal_padding = 5;
    let vertical_padding = 5;

    let stroke_width = 2;

    let rect_style = create_svg_elem("style", &document)?;

    let rect_style_class_name = "byteRect";
    rect_style.set_inner_html(
        format!(
            ".{} {{fill: red; stroke: orange; stroke-width:{}; fill-opacity:0.1; stroke-opacity: 0.6}}",
            rect_style_class_name,
            stroke_width
        )
        .as_str(),
    );
    grid.append_child(&rect_style)?;

    let mut rect_x = start_x;

    for col_idx in 0..nr_of_columns {
        let mut rect_y = start_y;
        for row_idx in 0..nr_of_rows {
            let rect = create_svg_rect(
                &document,
                rect_x,
                rect_y,
                rect_width,
                rect_height,
                rect_style_class_name,
            )?;
            grid.append_child(&rect)?;

            // Add the text to the center of the SVG rectangle
            let svg_text = create_svg_text(
                grid_content[row_idx][col_idx].as_str(),
                rect_x + rect_width / 2,
                rect_y + rect_height / 2,
                text_style_class_name,
                &document,
            )?;
            grid.append_child(&svg_text)?;

            rect_y += rect_height + vertical_padding;
        }
        rect_x += rect_width + horizontal_padding;
    }

    // Resize the grid to fit its containing rectangles
    let svg_width = start_x + nr_of_columns * (horizontal_padding + rect_width + stroke_width);
    let svg_height = start_y + nr_of_rows * (vertical_padding + rect_height + stroke_width);
    grid.set_attribute("width", &svg_width.to_string())?;
    grid.set_attribute("height", &svg_height.to_string())?;

    Ok(grid)
}

fn create_svg_rect(
    document: &Document,
    x: usize,
    y: usize,
    rect_width: usize,
    rect_height: usize,
    class: &str,
) -> Result<web_sys::Element, JsValue> {
    let rect = create_svg_elem("rect", document)?;
    rect.set_attribute("x", &x.to_string())?;
    rect.set_attribute("y", &y.to_string())?;
    rect.set_attribute("width", &rect_width.to_string())?;
    rect.set_attribute("height", &rect_height.to_string())?;
    rect.set_attribute("class", class)?;
    // Rounded corners
    rect.set_attribute("rx", "1%")?;
    rect.set_attribute("ry", "1%")?;
    Ok(rect)
}
