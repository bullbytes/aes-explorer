mod aes_decrypt;
mod aes_encrypt;
mod aes_test_data;

fn main() {
    println!("main started");
    let state = aes_test_data::TEST_STATE;
    let key = aes_test_data::TEST_KEY;
    aes_encrypt::aes_128_encrypt(state, key);
}
