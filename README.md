# AES Explorer
An interactive visualization of [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard). Work in progress.

## Build instructions

    # Just once
    cargo install wasm-pack
    # Build the application, including WebAssembly and JavaScript files
    wasm-pack build --target web

## Run instructions
Start a web server to serve the files `index.html`, `pkg/aes_explorer.js`, and `pkg/aes_explorer_bg.wasm`:

    # Just once
    cargo install https
    # Start the web server, listening to localhost:8000
    http

